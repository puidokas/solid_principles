﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solid_principles.principles
{
    class SRP : IRunnable
    {
        public string Name
        {
            get
            {
                return "Single Responsibility Principle (SRP)";
            }
        }

        public void Run()
        {
            Report report = new Report()
            {
                Title = "SampleReport",
                Content = "SampleContent"
            };

            ReportGenerator generator = new ReportGenerator();
            ReportSaver saver = new ReportSaver();

            generator.GenerateReport(report);

            saver.SaveToFile(report);
        }
    }

    // Before SRP
    //public class Report
    //{
    //    public string Title { get; set; }
    //    public string Content { get; set; }

    //    public void GenerateReport()
    //    {
    //        // Generate report logic
    //    }

    //    public void SaveToFile()
    //    {
    //        // Save to file logic
    //    }
    //}

    // After SRP
    public class Report
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }

    public class ReportGenerator
    {
        public void GenerateReport(Report report)
        {
            // Generate report logic
            Console.WriteLine($"Report with title = {report.Title} and content = {report.Content} generated");
        }
    }

    public class ReportSaver
    {
        public void SaveToFile(Report report)
        {
            // Save to file logic
            Console.WriteLine($"Report with title = {report.Title} and content = {report.Content} saved to file");
        }
    }

}
