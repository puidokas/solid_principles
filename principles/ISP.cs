﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solid_principles.principles
{
    class ISP : IRunnable
    {
        public string Name
        {
            get
            {
                return "Interface Segregation Principle (ISP)";
            }
        }

        public void Run()
        {
            Object[] objects = [
                new Robot(),
                new Human()
            ];

            foreach (Object obj in objects) {
                if (obj is IWorkable)
                {
                    ((IWorkable)obj).Work();
                }
                if (obj is IEatable)
                {
                    ((IEatable)obj).Eat();
                }
            }
        }
    }

    // Violating ISP
    //public interface IWorker
    //{
    //    void Work();
    //    void Eat();
    //}

    //public class Robot : IWorker
    //{
    //    public void Work()
    //    {
    //        // Work logic for a robot
    //    }

    //    public void Eat()
    //    {
    //        // Eat logic for a robot
    //    }
    //}

    // Adhering to ISP
    public interface IWorkable
    {
        void Work();
    }

    public interface IEatable
    {
        void Eat();
    }

    public class Robot : IWorkable
    {
        public void Work()
        {
            // Work logic for a robot
            Console.WriteLine($"{GetType().Name} is working");
        }
    }

    public class Human : IWorkable, IEatable
    {
        public void Work()
        {
            // Work logic for a human
            Console.WriteLine($"{GetType().Name} is working");
        }

        public void Eat()
        {
            // Eat logic for a human
            Console.WriteLine($"{GetType().Name} is eating");
        }
    }

}
