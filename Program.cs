﻿using solid_principles;
using solid_principles.principles;

class Program
{
    static public void Main()
    {

        IRunnable[] runnables = [
            new SRP(),
            new OCP(),
            new LSP(),
            new ISP(),
            new DIP(),
        ];

        foreach (IRunnable runnable in runnables)
        {
            Console.WriteLine($"Running {runnable.Name}");
            runnable.Run();
            Console.WriteLine();
        }
    }
}